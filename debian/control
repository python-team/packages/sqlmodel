Source: sqlmodel
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>,
Build-Depends: debhelper-compat (= 13),
               pybuild-plugin-pyproject,
               python3-all,
               python3-fastapi <!nocheck>,
               python3-httpx <!nocheck>,
               python3-poetry-core,
               python3-pydantic <!nocheck>,
               python3-pytest <!nocheck>,
               python3-requests <!nocheck>,
               python3-sqlalchemy (>= 1.4.45) <!nocheck>,
Standards-Version: 4.6.0.1
Testsuite: autopkgtest-pkg-python
Homepage: https://github.com/tiangolo/sqlmodel
Vcs-Git: https://salsa.debian.org/python-team/packages/sqlmodel.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/sqlmodel

Package: python3-sqlmodel
Architecture: all
Depends: python3-pydantic (>= 1.8.2),
         python3-sqlalchemy (>= 1.4.45),
         python3-typing-extensions,
         ${misc:Depends},
         ${python3:Depends},
Recommends: ${python3:Recommends},
Suggests: ${python3:Suggests},
Description: SQL databases in Python, designed for simplicity, compatibility, and robustness
 SQLModel is a library for interacting with SQL databases from Python code, with
 Python objects. It is designed to be intuitive, easy to use, highly compatible,
 and robust.
 .
 SQLModel is based on Python type annotations, and powered by Pydantic and
 SQLAlchemy.
 .
 The key features are:
 .
  * Intuitive to write: Great editor support. Completion everywhere. Less time
    debugging. Designed to be easy to use and learn. Less time reading docs.
  * Easy to use: It has sensible defaults and does a lot of work underneath to
    simplify the code you write.
  * Compatible: It is designed to be compatible with FastAPI, Pydantic, and
    SQLAlchemy.
  * Extensible: You have all the power of SQLAlchemy and Pydantic underneath.
  * Short: Minimize code duplication. A single type annotation does a lot of
    work. No need to duplicate models in SQLAlchemy and Pydantic.
 .
 SQLModel is designed to simplify interacting with SQL databases in FastAPI
 applications, it was created by the same author.
 .
 It combines SQLAlchemy and Pydantic and tries to simplify the code you write as
 much as possible, allowing you to reduce the code duplication to a minimum, but
 while getting the best developer experience possible.
 .
 SQLModel is, in fact, a thin layer on top of Pydantic and SQLAlchemy, carefully
 designed to be compatible with both.
